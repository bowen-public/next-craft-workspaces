#!/bin/sh

cd /app
./craft install/check

if [ $? == 0 ]; then
  ./craft clear-caches/data && \
  ./craft migrate/all && \
  ./craft project-config/apply && \
  chown -R www-data:www-data ./storage && \
  echo "craft was already installed" > runtime-log.txt
else
  echo "The craft install failed" > runtime-log.txt
  source .env && \
  ./craft setup/db-creds \
    --interactive=0 \
    --driver=$CRAFT_DB_DRIVER \
    --server=$CRAFT_DB_SERVER \
    --user=$CRAFT_DB_USER \
    --database=$CRAFT_DB_DATABASE \
    --port=$CRAFT_DB_PORT \
    --password=$CRAFT_DB_PASSWORD \
    --schema=$CRAFT_DB_SCHEMA && \
  ./craft install/craft \
    --interactive=0 \
    --email=devteam@bowenmedia.com \
    --language=en \
    --password=$CRAFT_ADMIN_PASSWORD \
    --site-name=$CRAFT_DB_DATABASE \
    --site-url=$PRIMARY_SITE_URL \
    --username=admin && \
  chown -R www-data:www-data ./storage && \
  echo "craft was successfully installed" > runtime-log.txt
fi
