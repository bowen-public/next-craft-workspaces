<?php
/**
 * Yii Application Config
 *
 * Edit this file at your own risk!
 *
 * The array returned by this file will get merged with
 * vendor/craftcms/cms/src/config/app.php and app.[web|console].php, when
 * Craft's bootstrap script is defining the configuration for the entire
 * application.
 *
 * You can define custom modules and system components, and even override the
 * built-in system components.
 *
 * If you want to modify the application config for *only* web requests or
 * *only* console requests, create an app.web.php or app.console.php file in
 * your config/ folder, alongside this one.
 */

use craft\helpers\App;

$all = [
  'id' => App::env('CRAFT_APP_ID') ?: 'CraftCMS'
];

if (App::env('CRAFT_ENVIRONMENT') !== 'dev') {
  return array_merge($all, [
    'components' => [
      'redis' => [
        'class' => yii\redis\Connection::class,
        'hostname' => App::env('REDIS_HOST'),
        'port' => 6379,
        'password' => ''
      ],
      'cache' => [
        'class' => yii\redis\Cache::class,
        'defaultDuration' => 86400,
        'keyPrefix' => App::env('REDIS_CACHE_PREFIX')
      ],
      'session' => function () {
        $config = craft\helpers\App::sessionConfig();
        $config['class'] = yii\redis\Session::class;

        return Craft::createObject($config);
      }
    ]
  ]);
}

return $all;
