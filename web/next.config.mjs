// @ts-check

const assetUrl = process.env.NEXT_PUBLIC_ASSET_URL;
const assetPrefix = assetUrl || undefined;

// export const locales = ["en", "es"];

/** @type {import('next').NextConfig} */
const config = {
  assetPrefix,
  output: "standalone",
  images: {
    remotePatterns: [
      {
        protocol: "https",
        hostname: process.env.DO_SPACES_HOST,
        port: ""
      }
    ]
  },
  async redirects() {
    return [
      {
        source: "/something",
        destination: "/somethingelse",
        permanent: true,
      },
    ];
  },
};

export default config;
