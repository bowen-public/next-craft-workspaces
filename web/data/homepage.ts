// import { graphql } from "@/gql";
// import { graphQLClient } from "@/gql/client";
// import { getKeyParts } from "@/lib/cache";
// import { getPreviewToken } from "@/lib/preview";
// import { QueryClient } from "@tanstack/react-query";
// import { unstable_cache } from "next/cache";
// import { notFound } from "next/navigation";

// const homepageDoc = graphql(`
//   query Homepage {
//     entries: homepageEntries {
//       ... on homepage_Entry {
//         id
//         title
//       }
//     }
//   }
// `);

// export async function homepageData() {
//   const token = getPreviewToken();
//   const tag = "homepage/homepage";
//   const keyParts = getKeyParts([tag, token]);
//   const getData = async function () {
//     const queryClient = new QueryClient();
//     const { entries } = await queryClient.fetchQuery({
//       queryKey: keyParts,
//       queryFn: async () => graphQLClient(token).request(homepageDoc),
//     });

//     if (!entries || !entries[0]) {
//       notFound();
//     }
//     return { entry: entries[0] };
//   };
//   if (token === null) {
//     const getCached = unstable_cache(getData, keyParts, { tags: [tag] });
//     return getCached();
//   }
//   return getData();
// }
