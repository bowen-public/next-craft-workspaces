import { cookies, draftMode } from "next/headers";

export async function getPreviewToken() {
  const { isEnabled } = await draftMode();
  const { get } = await cookies();
  const c = isEnabled ? get("craftpreviewtoken") : null;

  if (c) return c.value;
  return null;
}
