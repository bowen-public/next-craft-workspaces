import nodemailer, { SendMailOptions, Transporter } from "nodemailer";

export interface TransportOptions {
  host?: string;
  port?: number;
  auth?: {
    user?: string;
    pass?: string;
  };
}

const opts: TransportOptions = {
  host: process.env.SMTP_HOST,
  port: Number(process.env.SMTP_PORT) || 0,
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASS
  }
};

const defaultTransporter = nodemailer.createTransport(opts);

/**
 *
 * If no transport options are added one will be created for you using the following process env vars:
 * SMTP_HOST, SMTP_USER, SMTP_PASS, and SMTP_PORT
 * @example
 * ```ts
 * const res = await sendMail({
 *   to: "devteam@bowenmedia.com",
 *   subject: "hey",
 *   html: "<p>hey</p>"
 * });
 * if (res.success) {
 *  console.log("sent")
 * }
 * ```
 */
export async function sendMail(
  emailOptions?: SendMailOptions,
  transportOptions?: TransportOptions
): Promise<{
  success: boolean;
  message: string;
}> {
  let transporter = defaultTransporter;

  if (transportOptions) {
    transporter = nodemailer.createTransport(transportOptions);
  }

  return await send(transporter, emailOptions);
}

async function send(
  transporter: Transporter,
  params: SendMailOptions = {}
): Promise<{ success: boolean; message: string }> {
  try {
    await transporter.sendMail({
      ...params,
      to: params.to || process.env.DEFAULT_TO_EMAIL,
      from: params.from || process.env.DEFAULT_FROM_EMAIL
    });

    return {
      success: true,
      message: "Email sent"
    };
  } catch (err) {
    console.error(err);
    return {
      success: false,
      message: "Email failed to send"
    };
  }
}
