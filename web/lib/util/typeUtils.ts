export type PageParams<T> = { params: T };

export type ObjectValues<T> = T[keyof T];

export type Nullable<T> = { [K in keyof T]: T[K] | null };

export type RequiredNotNull<T> = {
  [P in keyof T]: NonNullable<T[P]>;
};

export type Ensure<T, K extends keyof T> = T & RequiredNotNull<Pick<T, K>>;

export type AwaitedReturn<T extends (...args: any) => any> = Awaited<
  ReturnType<T>
>;
