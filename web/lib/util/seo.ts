import { Metadata } from "next";

const defaultVerificators = ["google", "yahoo", "yandex", "me"];

type SeoProps =
  | {
      metaTitleContainer?: string | null | undefined;
      metaTagContainer?: string | null | undefined;
      metaLinkContainer?: string | null | undefined;
    }
  | null
  | undefined;

export function parseSeo(props: SeoProps) {
  if (!props) return {};

  const { metaLinkContainer, metaTagContainer, metaTitleContainer } = props;

  const parsedTitle = JSON.parse(metaTitleContainer || "{}");
  const parsedLinks = JSON.parse(metaLinkContainer || "{}");
  const parsedTags = JSON.parse(metaTagContainer || "{}");

  let seomatic: Metadata = {};

  if (!!parsedTitle?.title?.title) {
    seomatic = {
      ...seomatic,
      title: parsedTitle.title.title
    };
  }

  // all from parsedLinks
  seomatic = {
    ...seomatic,
    alternates: {
      canonical: parsedLinks?.canonical?.href,
      languages: parsedLinks?.alternate?.length
        ? parsedLinks?.alternate?.reduce((acc, cur) => {
            return {
              ...acc,
              [cur.hreflang]: cur.href
            };
          }, {})
        : parsedLinks?.alternate?.hreflang
          ? {
              [parsedLinks.alternate.hreflang]: parsedLinks.alternate.href
            }
          : null
    },
    authors: parsedLinks?.author?.href
      ? {
          url: parsedLinks.author.href,
          name: "Bowen Media"
        }
      : null
  };

  // all from parsedTags
  const verification = Object.keys(parsedTags)?.reduce((acc, cur) => {
    if (cur.includes("-site-verification")) {
      const name = cur.replace("-site-verification", "");

      if (defaultVerificators.includes(name)) {
        return {
          ...acc,
          [name]: parsedTags[cur]?.content
        };
      }

      return {
        ...acc,
        other: {
          ...("other" in acc && typeof acc.other === "object"
            ? {
                ...acc.other
              }
            : {}),
          [name]: parsedTags[cur]?.content
        }
      };
    }

    return acc;
  }, {});

  seomatic = {
    ...seomatic,
    generator: parsedTags?.generator,
    keywords: parsedTags?.keywords?.content,
    description: parsedTags?.description?.content,
    referrer: parsedTags?.referrer?.content,
    // not sure about robots
    robots: parsedTags?.robots?.content,
    openGraph: {
      locale: parsedTags?.["og:locale"].content,
      alternateLocale: parsedTags?.["og:locale:alternate"]?.length
        ? parsedTags["og:locale:alternate"].map((item) => item.content)
        : parsedTags?.["og:locale:alternate"]?.content
          ? parsedTags["og:locale:alternate"].content
          : undefined,
      siteName: parsedTags?.["og:site_name"]?.content,
      type: parsedTags?.["og:type"]?.content,
      url: parsedTags?.["og:url"]?.content,
      title: parsedTags?.["og:title"]?.content,
      description: parsedTags?.["og:description"]?.content,
      images: {
        url: parsedTags?.["og:image"]?.content,
        alt: parsedTags?.["og:image:alt"]?.content,
        width: parsedTags?.["og:image:width"]?.content,
        height: parsedTags?.["og:image:height"]?.content
      }
    },
    twitter: {
      card: parsedTags?.["twitter:card"]?.content,
      site: parsedTags?.["twitter:site"]?.content,
      creator: parsedTags?.["twitter:creator"]?.content,
      title: parsedTags?.["twitter:title"]?.content,
      description: parsedTags?.["twitter:description"]?.content,
      images: {
        url: parsedTags?.["twitter:image"]?.content,
        alt: parsedTags?.["twitter:image:alt"]?.content,
        width: parsedTags?.["twitter:image:width"]?.content,
        height: parsedTags?.["twitter:image:height"]?.content
      }
    },
    verification,
    other: {
      ...(parsedTags?.["fb:profile_id"]?.content
        ? { "fb:profile_id": parsedTags["fb:profile_id"].content }
        : {}),
      ...(parsedTags?.["fb:profile_id"]?.content
        ? { "fb:app_id": parsedTags["fb:profile_id"].content }
        : {})
    }
  };

  return seomatic;
}
