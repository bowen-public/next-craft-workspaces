import { GraphQLClient } from "graphql-request";

const mainUrl = process.env.API_CI_BUILD_URL as string;

export function graphQLClient(token?: string | null | undefined) {
  const url = token ? mainUrl + `?token=${token}` : mainUrl;

  return new GraphQLClient(url, {
    headers: {
      Authorization: `Bearer ${process.env.CRAFT_GRAPHQL_AUTH_TOKEN as string}`,
    },
  });
}
