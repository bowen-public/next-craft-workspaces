import type { Config } from "tailwindcss";

const minws = "1560px";
const maxdt = "1559px";
const mindt = "1240px";
const maxlt = "1239px";
const minlt = "944px";
const maxtb = "943px";
const mintb = "688px";
const maxph = "687px";

const config = {
  darkMode: ["class"],
  content: [
    "./pages/**/*.{ts,tsx}",
    "./components/**/*.{ts,tsx}",
    "./app/**/*.{ts,tsx}",
    "./src/**/*.{ts,tsx}",
  ],
  prefix: "",
  theme: {
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1400px",
      },
    },
    extend: {
      fontFamily: {
        acuminPro: ["acumin-pro", "serif"],
        utopiaStd: ["utopia-std", "serif"],
      },
      colors: {
        blue: "#254476",
        ocean: "#3564AD",
        yellow: "#FEC513",
        gold: "#C8A030",
        navy: "#182D4E",
        teal: "#BFD9DA",
        gray: "#F3F3F3",
        dkGray: "#333333",
        border: "hsl(var(--border))",
        input: "hsl(var(--input))",
        ring: "hsl(var(--ring))",
        background: "hsl(var(--background))",
        foreground: "hsl(var(--foreground))",
        primary: {
          DEFAULT: "hsl(var(--primary))",
          foreground: "hsl(var(--primary-foreground))",
        },
        secondary: {
          DEFAULT: "hsl(var(--secondary))",
          foreground: "hsl(var(--secondary-foreground))",
        },
        destructive: {
          DEFAULT: "hsl(var(--destructive))",
          foreground: "hsl(var(--destructive-foreground))",
        },
        muted: {
          DEFAULT: "hsl(var(--muted))",
          foreground: "hsl(var(--muted-foreground))",
        },
        accent: {
          DEFAULT: "hsl(var(--accent))",
          foreground: "hsl(var(--accent-foreground))",
        },
        popover: {
          DEFAULT: "hsl(var(--popover))",
          foreground: "hsl(var(--popover-foreground))",
        },
        card: {
          DEFAULT: "hsl(var(--card))",
          foreground: "hsl(var(--card-foreground))",
        },
      },
      borderRadius: {
        lg: "var(--radius)",
        md: "calc(var(--radius) - 2px)",
        sm: "calc(var(--radius) - 4px)",
      },
      keyframes: {
        "accordion-down": {
          from: { height: "0" },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: "0" },
        },
      },
      animation: {
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
      },
      backgroundImage: {
        blueGr: "linear-gradient(108deg,#254476 0%,#182D4E 100%)",
        blueGr2: "linear-gradient(90deg,#182D4E 0%,#3564AD 100%)",
      },
    },
    screens: {
      ////////////////////// max width and down
      dt: { max: maxdt }, // dt: max desktop
      lt: { max: maxlt }, // lt: max laptop
      tb: { max: maxtb }, // tb: max tablet
      ph: { max: maxph }, // ph: max phone
      ////////////////////// min width and up
      mintb: { min: mintb }, // min tablet
      minlt: { min: minlt }, // min laptop
      mindt: { min: mindt }, // min desktop
      minws: { min: minws }, // min widescreen
      ////////////////////// only middle breaks
      otb: { min: mintb, max: maxtb }, // only tablet
      olt: { min: minlt, max: maxlt }, // only laptop
      odt: { min: mindt, max: maxdt }, // only desktop
    },
  },
  plugins: [require("tailwindcss-animate")],
} satisfies Config;

export default config;
