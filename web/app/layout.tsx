import NextTopLoader from "nextjs-toploader";
import { PropsWithChildren } from "react";
import LivePreviewWarning from "./_components/preview/livePreviewWarning";
import "./globals.scss";
import Providers from "./providers";

export default function RootLayout(props: PropsWithChildren) {
  const { children } = props;

  return (
    <html lang="en">
      <body>
        <LivePreviewWarning />
        <NextTopLoader color="#0099ba" />
        <Providers>
          <main>{children}</main>
        </Providers>
      </body>
    </html>
  );
}
