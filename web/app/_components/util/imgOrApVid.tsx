import { cn } from "@/lib/utils";
import ImgWrap, { ImgAsset } from "./imgWrap";

export type ImgOrApVidProps = {
  asset: ImgAsset | undefined | null;
  wrapCn?: string;
  innerCn?: string;
};

export default function ImgOrApVid(props: ImgOrApVidProps) {
  const { asset, wrapCn, innerCn } = props;
  const { mimeType, url } = asset || {};

  if (mimeType?.startsWith("video") && url) {
    return (
      <div className={cn("relative", wrapCn)}>
        <video
          className={cn("absolute fit object-cover", innerCn)}
          src={url}
          autoPlay
          muted
        ></video>
      </div>
    );
  }
  return <ImgWrap wrapCn={wrapCn} innerCn={innerCn} img={asset} />;
}
