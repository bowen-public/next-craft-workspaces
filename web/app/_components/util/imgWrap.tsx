import { cn } from "@/lib/utils";
import Image, { ImageProps } from "next/image";
import { FC } from "react";

// for next 13

export type ImgAsset = {
  id?: string | null;
  url?: string | null;
  mimeType?: string | null;
  title?: string | null;
  width?: number | null;
  height?: number | null;
};

export type ImgProps = ImgAsset | undefined | null;

export type ImgWrapProps = Omit<
  ImageProps,
  "src" | "loader" | "alt" | "className" | "fill"
> & {
  img: ImgProps;
  wrapCn?: string;
  innerCn?: string;
};

const ImgWrap: FC<ImgWrapProps> = (props) => {
  const { wrapCn, innerCn, img, ...rest } = props;
  const url = img?.url;
  // const id = img?.id;
  // const mimeType = img?.mimeType;
  const alt = img?.title ?? "";

  if (url) {
    return (
      <div className={cn("relative", wrapCn)}>
        <Image
          {...rest}
          className={cn("object-cover", innerCn)}
          src={url}
          alt={alt}
          fill
        />
      </div>
    );
  }
  return <div className={wrapCn}></div>;
};

export default ImgWrap;
