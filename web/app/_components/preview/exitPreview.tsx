"use client";

export type ExitPreviewProps = {};

export default function ExitPreview(props: ExitPreviewProps) {
  const {} = props;

  return (
    <button
      className="font-bold underline"
      onClick={() => {
        fetch("/api/exit-preview").then(() => {
          location.reload();
        });
      }}
    >
      here
    </button>
  );
}
