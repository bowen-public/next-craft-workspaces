import { draftMode } from "next/headers";
import ExitPreview from "./exitPreview";

export default async function LivePreviewWarning() {
  const { isEnabled } = await draftMode();

  if (!isEnabled) return null;

  return (
    <div className="fixed left-0 top-0 z-[100] w-full bg-red-900 px-12 py-6 text-center text-[16px] text-white">
      You are in preview mode. If viewing this inside Craft CMS, ignore this
      message. Otherwise click <ExitPreview /> to exit if you want to view pages
      in your browser regularly.
    </div>
  );
}
