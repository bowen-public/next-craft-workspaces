import { revalidateTag } from "next/cache";
import { type NextRequest } from "next/server";

type Payload = {
  sender?: {
    sectionId?: number | null | undefined;
    slug?: string | null | undefined;
    ref?: string | null | undefined;
    // ref is a string which is of format [sectionHandle]/[slug]
    // basicPage/test-5
  };
};

export async function POST(req: NextRequest) {
  const auth = req.headers.get("auth");

  if (auth !== process.env.CRAFT_PREVIEW_SECRET) {
    return new Response("Invalid Token", { status: 401 });
  }

  const json: Payload = await req.json();

  if (
    "sender" in json &&
    json.sender &&
    typeof json.sender === "object" &&
    "ref" in json.sender &&
    typeof json.sender.ref === "string"
  ) {
    const ref = json.sender.ref;
    const refs = ref.split("/");
    if (refs.length === 2) {
      console.log("revalidating tag", refs[0]);
      revalidateTag(refs[0]);
    }
    console.log("revalidating tag", ref);
    revalidateTag(ref);
  }

  return new Response("", { status: 200 });
}
