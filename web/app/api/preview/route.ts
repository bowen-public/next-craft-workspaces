import { cookies, draftMode } from "next/headers";
import { redirect } from "next/navigation";
import { NextRequest } from "next/server";

export async function GET(req: NextRequest) {
  const { enable } = await draftMode();
  const { set } = await cookies();
  const url = process.env.NEXT_PUBLIC_MAIN_SITE_URL as string;
  const secret = req.nextUrl.searchParams.get("secret");
  const qUri = req.nextUrl.searchParams.get("uri");
  const token = req.nextUrl.searchParams.get("token");
  const uri = qUri === "__home__" ? "" : qUri;
  const path = "/" + uri;
  const fullUrl = url + path;

  if (secret !== process.env.CRAFT_PREVIEW_SECRET || !qUri) {
    return new Response("Invalid Token", { status: 401 });
  }

  set("craftpreviewtoken", token ?? "");
  enable();
  redirect(fullUrl);
}
